package com.gi.panel;

import java.awt.Color;

import javax.swing.JPanel;

import com.gi.dao.ProductDao;
import com.gi.model.Product;
import com.gi.util.Constant;
import com.gi.util.Hellper;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JFrame;

public class InputProductPanel extends JPanel {
	private JTextField txtId;
	private JTextField txtProduct;
	private JTextField txtJumlah;
	private JTextField txtHarga;
	private JTextField txtTgl;
	private JTextField txtSupplier;
	private JTextArea txtDeskripsi;
	private JComboBox cmbSatuan;

	JFrame frame = new JFrame();
	public InputProductPanel() {
		setBackground(Color.LIGHT_GRAY);
		setBounds(0, 0, Constant.Panel_Width, Constant.Panel_Height);
		setLayout(null);
		setVisible(false);
		
		JLabel lblTitleInput = new JLabel("Input Data Produk");
		lblTitleInput.setFont(new Font("Arial Black", Font.BOLD, 18));
		lblTitleInput.setBounds(345, 83, 195, 35);
		add(lblTitleInput, CENTER_ALIGNMENT);
		
		JLabel lblId = new JLabel("Id Barang");
		lblId.setBounds(87, 166, 69, 14);
		add(lblId);
		
		JLabel lblNamaProduct = new JLabel("Nama Product");
		lblNamaProduct.setBounds(87, 215, 90, 14);
		add(lblNamaProduct);
		
		JLabel lblJumlah = new JLabel("Jumlah");
		lblJumlah.setBounds(87, 264, 69, 14);
		add(lblJumlah);
		
		JLabel lblHarga = new JLabel("Harga Satuan");
		lblHarga.setBounds(87, 311, 79, 14);
		add(lblHarga);
		
		JLabel lblTglMasuk = new JLabel("Tanggal Masuk");
		lblTglMasuk.setBounds(441, 166, 90, 14);
		add(lblTglMasuk);
		
		JLabel lblSupplier = new JLabel("Supplier");
		lblSupplier.setBounds(441, 215, 69, 14);
		add(lblSupplier);
		
		JLabel lblSatuan = new JLabel("Satuan");
		lblSatuan.setBounds(441, 264, 62, 14);
		add(lblSatuan);
		
		JLabel lblDeskripsi = new JLabel("Deskripsi");
		lblDeskripsi.setBounds(441, 313, 69, 14);
		add(lblDeskripsi);
		
		txtId = new JTextField();
		txtId.setBounds(205, 163, 180, 20);
		add(txtId);
		txtId.setColumns(10);
		
		txtProduct = new JTextField();
		txtProduct.setBounds(205, 209, 180, 20);
		add(txtProduct);
		txtProduct.setColumns(10);
		
		txtJumlah = new JTextField();
		txtJumlah.setBounds(205, 258, 180, 20);
		add(txtJumlah);
		txtJumlah.setColumns(10);
		
		txtHarga = new JTextField();
		txtHarga.setBounds(205, 305, 180, 20);
		add(txtHarga);
		txtHarga.setColumns(10);
		
		txtTgl = new JTextField();
		txtTgl.setEditable(false);
		txtTgl.setBounds(598, 160, 180, 20);
		add(txtTgl);
		txtTgl.setText(Hellper.currentDate());
		txtTgl.setColumns(10);
		
		txtSupplier = new JTextField();
		txtSupplier.setBounds(598, 209, 180, 20);
		add(txtSupplier);
		txtSupplier.setColumns(10);
		
		cmbSatuan = new JComboBox();
		cmbSatuan.setModel(new DefaultComboBoxModel(Constant.Satuan));
		cmbSatuan.setBounds(598, 256, 180, 24);
		add(cmbSatuan);
		
		txtDeskripsi = new JTextArea();
		txtDeskripsi.setBounds(598, 305, 180, 58);
		add(txtDeskripsi);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(validation()) {
					ProductDao productDao = new ProductDao();
					Product product = (Product) productDao.getDataByVisibleCode(txtId.getText());
					
					if(product == null) {
						product = new Product();
						product.setId_produk(Long.parseLong(txtId.getText()));
					}
					product.setNama_product(txtProduct.getText());
					product.setStock(Long.parseLong(txtJumlah.getText()));
					product.setHarga_satuan(Long.parseLong(txtHarga.getText()));
					product.setSupplier(txtSupplier.getText());
					String satuan = cmbSatuan.getSelectedItem().toString();
					product.setSatuan(satuan);
					product.setDeskripsi(txtDeskripsi.getText());
					
					productDao.saveOrUpdate(product);
					
					JOptionPane.showMessageDialog(null, "Data Baru Ditambahkan", "Success", JOptionPane.INFORMATION_MESSAGE);
					setFormEmpty();
				}
			}
		});
		btnSubmit.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		btnSubmit.setBounds(376, 428, 121, 35);
		add(btnSubmit);
		
	}
	private boolean validation() {
		boolean valid = true;
		JTextField field[] = {txtId, txtProduct, txtJumlah, txtHarga, txtTgl, txtSupplier};
		JTextArea text[] = {txtDeskripsi};
		
		for(int i = 0; i < field.length; i++) {
			if(field[i].getText().equalsIgnoreCase("")) {
				valid = false;
				break;
			}
		}
		for(int i = 0; i < text.length; i++) {
			if(text[i].getText().equalsIgnoreCase("")) {
				valid = false;
				break;
			}
		}
		if(!valid) {
			JOptionPane.showConfirmDialog(null, "Form Harus Di Isi lengkap", "Gagal Menambahkan", JOptionPane.NO_OPTION);
		}
		return valid;
	}
	private void setFormEmpty() {
		JTextField field[] = {txtId, txtProduct, txtJumlah, txtHarga, txtSupplier};
		JTextArea text[] = {txtDeskripsi};
		
		for(int i = 0; i < field.length; i++) {
			field[i].setText("");
		}
		for(int i = 0; i < text.length; i++) {
			text[i].setText("");
		}
	}
}
