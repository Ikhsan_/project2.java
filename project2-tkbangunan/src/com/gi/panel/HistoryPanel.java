package com.gi.panel;

import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.MessageFormat;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.gi.dao.TransaksiDao;
import com.gi.model.Transaksi;
import com.gi.util.Constant;
import javax.swing.JButton;

public class HistoryPanel extends JPanel {
	private List listTrans;
	private String[][] arrHistory;
	private JTable tblHistory;


	public HistoryPanel() {
		setBackground(Color.LIGHT_GRAY);
		setBounds(0, 0, Constant.Panel_Width, Constant.Panel_Height);
		setLayout(null);

		TransaksiDao transaksiDao = new TransaksiDao();
		listTrans = transaksiDao.getAllData();
		Object headHis[] = {"id Transaksi", "id Product", "Nama Poduct", "Tanggal", "Ket", "Jumlah", "Satuan", "Total Harga"};
		arrHistory = new String[listTrans.size() +1][8];

		for(int i = 0; i < listTrans.size(); i++) {
			Transaksi transaksi = (Transaksi) listTrans.get(i);
			arrHistory[i+0][0] = transaksi.getId_transaksi().toString();
			arrHistory[i+0][1] = transaksi.getId_product().toString();
			arrHistory[i+0][2] = transaksi.getNama_product();
			arrHistory[i+0][3] = transaksi.getTanggal().toString();
			arrHistory[i+0][4] = transaksi.getStatus();
			arrHistory[i+0][5] = transaksi.getJumlah().toString();
			arrHistory[i+0][6] = transaksi.getSatuan();
			arrHistory[i+0][7] = transaksi.getHarga().toString();
		}
		
		JScrollPane spHistory = new JScrollPane();
		spHistory.setBounds(96, 152, 714, 192);
		add(spHistory);

		tblHistory = new JTable();
		tblHistory.setModel(new DefaultTableModel(arrHistory, headHis));
		tblHistory.getColumnModel().getColumn(0).setPreferredWidth(35);
		tblHistory.getColumnModel().getColumn(1).setPreferredWidth(35);
		tblHistory.getColumnModel().getColumn(2).setPreferredWidth(35);
		tblHistory.getColumnModel().getColumn(3).setPreferredWidth(35);
		tblHistory.getColumnModel().getColumn(4).setPreferredWidth(35);
		tblHistory.getColumnModel().getColumn(5).setPreferredWidth(35);
		tblHistory.getColumnModel().getColumn(6).setPreferredWidth(35);
		tblHistory.getColumnModel().getColumn(7).setPreferredWidth(35);
		spHistory.setViewportView(tblHistory);
		
		JLabel lblNewLabel = new JLabel("History Transaksi");
		lblNewLabel.setFont(new Font("Arial Black", Font.BOLD, 16));
		lblNewLabel.setBounds(362, 97, 303, 21);
		add(lblNewLabel);
		
		Button button = new Button("Refresh");
		button.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tblHistory.clearSelection();
				listTrans = transaksiDao.getAllData();
				arrHistory = new String[listTrans.size()][8];
				for(int i = 0; i < listTrans.size(); i++) {
					Transaksi transaksi = (Transaksi) listTrans.get(i);
					//String shoeDate = formatter
					arrHistory[i+0][0] = transaksi.getId_transaksi().toString();
					arrHistory[i+0][1] = transaksi.getId_product().toString();
					arrHistory[i+0][2] = transaksi.getNama_product();
					arrHistory[i+0][3] = transaksi.getTanggal().toString();
					arrHistory[i+0][4] = transaksi.getStatus();
					arrHistory[i+0][5] = transaksi.getJumlah().toString();
					arrHistory[i+0][6] = transaksi.getSatuan();
					arrHistory[i+0][7] = transaksi.getHarga().toString();
				}
				tblHistory.setModel(new DefaultTableModel(arrHistory,headHis));
			}
		});
		button.setBounds(682, 388, 128, 39);
		add(button);
		
		JButton btnPrint = new JButton("Print");
		btnPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MessageFormat header = new MessageFormat("History Keluar Masuk Barang");
				MessageFormat total = new MessageFormat("Tetap Semangat");
				try {
					
					tblHistory.print(JTable.PrintMode.FIT_WIDTH, header, total);
					
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, "Gagal Cetak");
				}
			}
		});
		btnPrint.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnPrint.setBounds(96, 388, 106, 39);
		add(btnPrint);
		
		setVisible(false);
	}
}
