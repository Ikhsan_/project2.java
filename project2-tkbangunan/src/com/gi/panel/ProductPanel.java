package com.gi.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.gi.dao.ProductDao;
import com.gi.model.Product;
import com.gi.util.Constant;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ProductPanel extends JPanel {
	private JTable tableProduct;
	private Object[][] arrProduct;
	@SuppressWarnings("rawtypes")
	private List listProduct;
	private JTextField txtNamaPro;
	private JTextField txtStock;
	private JTextField textHarga;
	private JTextField textSupplier;

	public ProductPanel() {
		setBackground(Color.LIGHT_GRAY);
		setBounds(0, 0, Constant.Panel_Width, Constant.Panel_Height);
		setLayout(null);
		setVisible(false);
		
		ProductDao productDao = new ProductDao();
		listProduct = productDao.getAllData();
		Object headers[] = {"Id", "Nama Product", "Stock", "Harga", "Tanggal Masuk", "Supplier","Satuan", "Deskripsi"};
		
		arrProduct = new String[listProduct.size() +1][8];
		for (int i = 0; i < listProduct.size(); i++) {
			Product product = (Product) listProduct.get(i);
			arrProduct[i+0][0] = product.getId_produk().toString();
			arrProduct[i+0][1] = product.getNama_product();
			arrProduct[i+0][2] = product.getStock().toString();
			arrProduct[i+0][3] = product.getHarga_satuan().toString();
			arrProduct[i+0][4] = product.getTgl_masuk().toString();
			arrProduct[i+0][5] = product.getSupplier();
			arrProduct[i+0][6] = product.getSatuan();
			arrProduct[i+0][7] = product.getDeskripsi();	
		}
		
		JScrollPane scrollPane = new JScrollPane(tableProduct);
		scrollPane.setBounds(49, 96, 799, 193);
		add(scrollPane);
		
		JLabel lblTitleProduk = new JLabel("List Produk");
		lblTitleProduk.setFont(new Font("Arial Black", Font.BOLD, 16));
		lblTitleProduk.setBounds(368, 41, 124, 21);
		add(lblTitleProduk);
		
		tableProduct = new JTable();
		tableProduct.setModel(new DefaultTableModel(
			arrProduct,headers
		));
		tableProduct.getColumnModel().getColumn(0).setPreferredWidth(34);
		tableProduct.getColumnModel().getColumn(2).setPreferredWidth(79);
		tableProduct.getColumnModel().getColumn(3).setPreferredWidth(54);
		tableProduct.getColumnModel().getColumn(4).setPreferredWidth(80);
		tableProduct.getColumnModel().getColumn(5).setPreferredWidth(84);
		scrollPane.setViewportView(tableProduct);
		tableProduct.setToolTipText("Tabel Product");
		tableProduct.setBounds(12, 49, 588, 120);
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tableProduct.clearSelection();
				listProduct = productDao.getAllData();
				arrProduct = new String[listProduct.size() +1][8];
				for (int i = 0; i < listProduct.size(); i++) {
					Product product = (Product) listProduct.get(i);
					arrProduct[i+0][0] = product.getId_produk().toString();
					arrProduct[i+0][1] = product.getNama_product();
					arrProduct[i+0][2] = product.getStock().toString();
					arrProduct[i+0][3] = product.getHarga_satuan().toString();
					arrProduct[i+0][4] = product.getTgl_masuk().toString();
					arrProduct[i+0][5] = product.getSupplier();
					arrProduct[i+0][6] = product.getSatuan();
					arrProduct[i+0][7] = product.getDeskripsi();	
				}
				tableProduct.setModel(new DefaultTableModel(arrProduct, headers));
			}
		});
		btnRefresh.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnRefresh.setBounds(406, 313, 103, 36);
		add(btnRefresh);
		
		JButton btnExport = new JButton("Export");
		btnExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				FileOutputStream excelFos = null;
//		        XSSFWorkbook excelJTableExport = null;
//		        BufferedOutputStream excelBos = null;
//		        try {
//		 
//		            JFileChooser excelFileChooser = new JFileChooser("E:\\ikhsan");
//		            excelFileChooser.setDialogTitle("Save As ..");
//
//		            FileNameExtensionFilter fnef = new FileNameExtensionFilter("Files", "xls", "xlsx", "xlsm");
//
//		            excelFileChooser.setFileFilter(fnef);
//		            int chooser = excelFileChooser.showSaveDialog(null);
//
//		            if (chooser == JFileChooser.APPROVE_OPTION) {
//
//		                excelJTableExport = new XSSFWorkbook();
//		                XSSFSheet excelSheet = excelJTableExport.createSheet("Jtable Export");
//
//		                for (int i = 0; i < tableProduct.getRowCount(); i++) {
//		                    XSSFRow excelRow = excelSheet.createRow(i);
//		                    if (i == tableProduct.getRowCount()) {
//								break;
//							}
//		                    for (int j = 0; j < tableProduct.getColumnCount(); j++) {
//		                        XSSFCell excelCell = excelRow.createCell(j);
//		 
//		                        excelCell.setCellValue(tableProduct.getValueAt(i, j).toString());
//		                        System.out.println("row : " +tableProduct.getRowCount() + "Col:" + tableProduct.getColumnCount());
//		                    }
//		                }
//		                
//		                excelFos = new FileOutputStream(excelFileChooser.getSelectedFile() + ".xlsx");
//		                excelBos = new BufferedOutputStream(excelFos);
//		                excelJTableExport.write(excelBos);
//		                JOptionPane.showMessageDialog(null, "Exported Successfully");
//		            }
//		 
//		        } catch (FileNotFoundException ex) {
//		            JOptionPane.showMessageDialog(null, ex);
//		        } catch (IOException ex) {
//		            JOptionPane.showMessageDialog(null, ex);
//		        } finally {
//		            try {
//		                if (excelFos != null) {
//		                    excelFos.close();
//		                }
//		                if (excelBos != null) {
//		                    excelBos.close();
//		                }
//		                if (excelJTableExport != null) {
//		                    excelJTableExport.close();
//		                }
//		            } catch (IOException ex) {
//		                JOptionPane.showMessageDialog(null, ex);
//		            }
//		        }
				MessageFormat header = new MessageFormat("List Product");
				MessageFormat total = new MessageFormat("Semangat Boss");
				try {
					
					tableProduct.print(JTable.PrintMode.FIT_WIDTH, header, total);
					
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, "Gagal Cetak");
				}
			}
		});
		btnExport.setBounds(117, 315, 114, 36);
		add(btnExport);
		
		JLabel lblUnama = new JLabel("Nama Product");
		lblUnama.setBounds(68, 397, 95, 21);
		add(lblUnama);
		
		JLabel lblStock = new JLabel("Stock");
		lblStock.setBounds(68, 460, 95, 21);
		add(lblStock);
		
		txtNamaPro = new JTextField();
		txtNamaPro.setBounds(194, 397, 171, 20);
		add(txtNamaPro);
		txtNamaPro.setColumns(10);
		
		txtStock = new JTextField();
		txtStock.setBounds(194, 460, 171, 20);
		add(txtStock);
		txtStock.setColumns(10);
		
		JLabel lblHarga = new JLabel("Harga");
		lblHarga.setBounds(512, 397, 59, 14);
		add(lblHarga);
		
		JLabel lblSuplier = new JLabel("Supplier");
		lblSuplier.setBounds(512, 460, 59, 14);
		add(lblSuplier);
		
		textHarga = new JTextField();
		textHarga.setBounds(621, 394, 171, 20);
		add(textHarga);
		textHarga.setColumns(10);
		
		JLabel lblId = new JLabel("");
		lblId.setForeground(Color.LIGHT_GRAY);
		lblId.setBounds(378, 430, 114, 14);
		add(lblId);
		
		
		textSupplier = new JTextField();
		textSupplier.setBounds(621, 457, 171, 20);
		add(textSupplier);
		textSupplier.setColumns(10);
		
		tableProduct.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				ProductDao productDao = new ProductDao();
				try {
					String code  = tableProduct.getValueAt(tableProduct.getSelectedRow(), 0).toString();
					Product product = (Product) productDao.getDataById(Long.parseLong(code));
					if (product !=null) {
						lblId.setText(product.getId_produk().toString());
						txtNamaPro.setText(product.getNama_product());
						txtStock.setText(product.getStock().toString());
						textHarga.setText(product.getHarga_satuan().toString());
						textSupplier.setText(product.getSupplier());
					}
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		});
		
		JButton btnEdit = new JButton("Update");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(validation()) {
					ProductDao productDao = new ProductDao();
					Product product = (Product) productDao.getDataById(Long.parseLong(lblId.getText()));
					
					product.setNama_product(txtNamaPro.getText());
					product.setStock(Long.parseLong(txtStock.getText()));
					product.setHarga_satuan(Long.parseLong(textHarga.getText()));
					product.setSupplier(textSupplier.getText());
					product.setId_produk(Long.parseLong(lblId.getText()));
					
					productDao.saveOrUpdate(product);
					JOptionPane.showMessageDialog(null, "Data berhasil di Update", "Update", JOptionPane.INFORMATION_MESSAGE);
					
					empty();
				}
			}
		});
		btnEdit.setBounds(380, 502, 103, 36);
		add(btnEdit);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProductDao dao = new ProductDao();
				dao.deleteDataById(Long.parseLong(lblId.getText()));
				JOptionPane.showMessageDialog(null, "Data Berhasil di Hapus", "Success", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		btnDelete.setBounds(643, 315, 103, 36);
		add(btnDelete);
		
	}
	
	public boolean validation() {
		boolean valid = true;
		
		JTextField field[] = {txtNamaPro, txtStock, textHarga, textSupplier};
		
		for (int i = 0; i < field.length; i++) {
			if (field[i].getText().equalsIgnoreCase("")) {
				valid= false;
				break;
			}
		}
		
		if(!valid) {
			JOptionPane.showMessageDialog(null, "Tolong Isi Perbaikan dengan lengkap", "Caution", JOptionPane.INFORMATION_MESSAGE);
		}
		
		return valid;
	}
	
	public void empty() {
		JTextField field[] = {txtNamaPro, txtStock, textHarga, textSupplier};
		
		for (int i = 0; i < field.length; i++) {
			field[i].setText("");
		}
	}
}



















