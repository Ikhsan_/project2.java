package com.gi.panel;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;import javax.swing.JTable.PrintMode;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import com.gi.dao.ProductDao;
import com.gi.dao.TransaksiDao;
import com.gi.main.Dashboard;
import com.gi.model.Product;
import com.gi.model.Terjual;
import com.gi.model.Transaksi;
import com.gi.util.Constant;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.text.MessageFormat;

import javax.swing.JTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class TerjualPanel extends JPanel {
	
	private JTable tableProduct, tabelBeli;
	private Object[][] arrProduct, arrBeli;
	private List listProduct;
	private JScrollPane spProduk, sptrans;
	private JTextField txtJumlah;
	private JTextField txtBarang;
	private JTextField txtHarJum;
	private JTextField txtBayar;
	private JTextField textKembali;

	public TerjualPanel() {
		setBackground(Color.LIGHT_GRAY);
		setBounds(0, 0, Constant.Panel_Width, Constant.Panel_Height);
		setLayout(null);
		List data = new ArrayList();
		
		JLabel lblTitleProduk = new JLabel("Beli Produk");
		lblTitleProduk.setFont(new Font("Arial Black", Font.BOLD, 16));
		lblTitleProduk.setBounds(371, 25, 124, 21);
		add(lblTitleProduk);
		
		ProductDao productDao = new ProductDao();
		listProduct = productDao.getAllData();
		Object headers[] = {"Code", "Nama Product", "Stock", "Harga Satuan", "Satuan"},
				headBeli[] = {"Nama Product", "Harga Satuan", "Satuan", "Jumlah", "Total", "code"};

		arrProduct = new String[listProduct.size() +1][5];
		for (int i = 0; i < listProduct.size(); i++) {
			Product product = (Product) listProduct.get(i);
			arrProduct[i+0][0] = product.getCode();
			arrProduct[i+0][1] = product.getNama_product();
			arrProduct[i+0][2] = product.getStock().toString();
			arrProduct[i+0][3] = product.getHarga_satuan().toString();
			arrProduct[i+0][4] = product.getSatuan();
		}
		
		spProduk = new JScrollPane(tableProduct);
		spProduk.setBounds(47, 70, 779, 124);
		add(spProduk);

		tableProduct = new JTable();
		tableProduct.setModel(new DefaultTableModel(
			arrProduct, headers
		));
		tableProduct.getColumnModel().getColumn(0).setPreferredWidth(34);
		tableProduct.getColumnModel().getColumn(1).setPreferredWidth(55);
		tableProduct.getColumnModel().getColumn(2).setPreferredWidth(55);
		tableProduct.getColumnModel().getColumn(3).setPreferredWidth(55);
		tableProduct.getColumnModel().getColumn(4).setPreferredWidth(55);
		spProduk.setViewportView(tableProduct);
		tableProduct.setToolTipText("Tabel Product");
		tableProduct.setBounds(12, 49, 588, 120);
		
		Button btnRefresh = new Button("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tableProduct.clearSelection();
				listProduct = productDao.getAllData();
				arrProduct = new String[listProduct.size()+1][5];
				int i = 0;
				while(i < listProduct.size()) {
					System.out.println(i + " product size " + listProduct.size());
					Product product = (Product) listProduct.get(i);
					arrProduct[i+0][0] = product.getCode();
					arrProduct[i+0][1] = product.getNama_product();
					arrProduct[i+0][2] = product.getStock().toString();
					arrProduct[i+0][3] = product.getHarga_satuan().toString();
					arrProduct[i+0][4] = product.getSatuan();
					
					i++;
				}
				tableProduct.setModel(new DefaultTableModel(arrProduct, headers));
			}
		});
		btnRefresh.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnRefresh.setBounds(371, 205, 103, 36);
		add(btnRefresh);
		
		JLabel lblNamaBarang = new JLabel("Nama Barang :");
		lblNamaBarang.setBounds(148, 258, 110, 29);
		add(lblNamaBarang);
		
		txtBarang = new JTextField();
		txtBarang.setBounds(148, 287, 119, 29);
		txtBarang.setEditable(false);
		add(txtBarang);
		txtBarang.setColumns(10);
		
		JLabel lblJml = new JLabel("Jumlah Beli :");
		lblJml.setBounds(385, 258, 110, 29);
		add(lblJml);
		
		txtJumlah = new JTextField();
		txtJumlah.setBounds(386, 286, 119, 30);
		add(txtJumlah);
		txtJumlah.setColumns(10);
		
		JLabel lblCode = new JLabel("");
		lblCode.setForeground(Color.LIGHT_GRAY);
		lblCode.setBounds(10, 287, 124, 21);
		add(lblCode);
		
		JLabel lblStock = new JLabel("");
		lblStock.setForeground(Color.LIGHT_GRAY);
		lblStock.setBounds(10, 343, 46, 14);
		add(lblStock);
		
		JLabel lblHarJum = new JLabel("Jumlah Harga");
		lblHarJum.setBounds(228, 458, 89, 21);
		add(lblHarJum);
		
		txtHarJum = new JTextField();
		txtHarJum.setBounds(349, 455, 146, 20);
		txtHarJum.setEditable(false);
		add(txtHarJum);
		txtHarJum.setColumns(10);

		tableProduct.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent event) {
//				System.out.println(event + " : ini event");
				ProductDao trans = new ProductDao();
				try {
					String code = tableProduct.getValueAt(tableProduct.getSelectedRow(), 0).toString();
					Product product = (Product) trans.getDataByVisibleCode(code);
					if(product != null) {
//						Terjual terjual = new Terjual();
//						terjual.setNama(product.getNama_product());
//						terjual.setHarga(product.getHarga_satuan());
//						terjual.setSatuan(product.getSatuan());
//						data.add(terjual);
						txtBarang.setText(product.getNama_product());
						lblCode.setText(product.getCode());
						lblStock.setText(product.getStock().toString());
					}else {
						
					}
				} catch (Exception mException) {
					mException.printStackTrace();
				}
			}
		});

		sptrans = new JScrollPane();
		sptrans.setBounds(148, 343, 578, 89);
		add(sptrans);

		tabelBeli = new JTable();
		tabelBeli.setModel(new DefaultTableModel(
			arrBeli,
			headBeli
		));
		tabelBeli.getColumnModel().getColumn(0).setPreferredWidth(34);
		tabelBeli.getColumnModel().getColumn(1).setPreferredWidth(79);
		tabelBeli.getColumnModel().getColumn(2).setPreferredWidth(54);
		tabelBeli.getColumnModel().getColumn(3).setPreferredWidth(79);
		tabelBeli.getColumnModel().getColumn(4).setPreferredWidth(54);
		tabelBeli.getColumnModel().getColumn(5).setPreferredWidth(0);
		sptrans.setViewportView(tabelBeli);
		tabelBeli.setToolTipText("Tabel Beli");
		tabelBeli.setBounds(12, 859, 588, 120);
		
		JButton btnAdd = new JButton("ADD");
		btnAdd.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent event) {
				if(validasi()) {
					ProductDao pdao = new ProductDao();
					TransaksiDao transaksiDao = new TransaksiDao();
					Product product = (Product) pdao.getDataByVisibleCode(lblCode.getText());
					String harga = product.getHarga_satuan().toString();
					Integer hr = Integer.parseInt(harga);
					Integer jum = Integer.parseInt(txtJumlah.getText());
					
					if(jum <= Long.parseLong(lblStock.getText())) {
					
					Integer total = hr * jum;
					String akhir = total.toString();
					Terjual terjual = new Terjual();
					terjual.setId_product(product.getId_produk());
					terjual.setNama(product.getNama_product());
					terjual.setHarga(product.getHarga_satuan());
					terjual.setSatuan(product.getSatuan());
					terjual.setJumlah(Long.parseLong(txtJumlah.getText()));
					terjual.setTotal(Long.parseLong(akhir));
					terjual.setCode(product.getCode());
					transaksiDao.saveOrUpdate(terjual);
					data.add(terjual);
					if (data != null) {
						arrBeli = new String[data.size()][6];
						for(int c = 0; c < data.size(); c++) {
							Terjual terjualin = (Terjual) data.get(c);
							arrBeli[c+0][0] = terjualin.getNama();
							arrBeli[c+0][1] = terjualin.getHarga().toString();
							arrBeli[c+0][2] = terjualin.getSatuan();
							arrBeli[c+0][3] = terjualin.getJumlah().toString();
							arrBeli[c+0][4] = terjualin.getTotal().toString();
							arrBeli[c+0][5] = terjualin.getCode();
						}
					}
					tabelBeli.setModel(new DefaultTableModel(arrBeli, headBeli));
					empty();
					int harJum = 0;
					for(int i = 0; i < tabelBeli.getRowCount(); i++) {
						Long getJum = Long.parseLong((String)tabelBeli.getValueAt(i, 4));
						harJum += getJum;
					}
					txtHarJum.setText(""+harJum);
					}else {
						JOptionPane.showMessageDialog(null, "Mohon maaf stock kami belum mencukupi", "Stock Terbatas", JOptionPane.INFORMATION_MESSAGE);
						txtJumlah.setText("");
					}
				}
			}
		});
		btnAdd.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnAdd.setBounds(623, 281, 103, 36);
		add(btnAdd);

		JLabel lblBayar = new JLabel("Bayar");
		lblBayar.setBounds(228, 496, 67, 21);
		add(lblBayar);
		
		JLabel lblKembalian = new JLabel("Kembalian");
		lblKembalian.setBounds(228, 540, 89, 14);
		add(lblKembalian);
		
		txtBayar = new JTextField();
		txtBayar.setBounds(348, 496, 147, 20);
		add(txtBayar);
		txtBayar.setColumns(10);
		
		textKembali = new JTextField();
		textKembali.setBounds(348, 537, 147, 20);
		add(textKembali);
		textKembali.setEditable(false);
		textKembali.setColumns(10);
	
		JButton btnCal = new JButton("Bayar");
		btnCal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				for (int i = 0; i < tabelBeli.getRowCount(); i++) {
					String code = (String)tabelBeli.getValueAt(i, 5).toString();
//					Long skur = Long.parseLong((String)tabelBeli.getValueAt(i, 3));
					ProductDao productDao = new ProductDao();
					TransaksiDao transaksiDao = new TransaksiDao();
					Product product = (Product) productDao.getDataByVisibleCode(code);
					
					product.setId_produk(product.getId_produk());
					product.setNama_product(product.getNama_product());
					product.setSatuan(product.getSatuan());
//					Long akhir = (long) 0;
//					for (int j = 0; j < product.; j++) {
//						Long awal = product.setStock(product.getStock());
//						akhir = awal - skur;
//					}
					for (int j = 0; j < tabelBeli.getRowCount(); j++) {
						
					}
				}
				
				calculate();
				if(textKembali.getText() != null && !textKembali.getText().trim().equalsIgnoreCase("")) {
					int opsi = JOptionPane.showConfirmDialog(null, "Ingin Cetak Struk?", "Terimakasih sudah berbelanja", JOptionPane.YES_NO_OPTION);
					if(opsi == JOptionPane.YES_OPTION) {
						MessageFormat header = new MessageFormat("Total Belanja");
						MessageFormat total = new MessageFormat("Terimakasih sudah berbelanja");
						try {
							tabelBeli.print(JTable.PrintMode.FIT_WIDTH, header, total);
						} catch (Exception e2) {
							JOptionPane.showMessageDialog(null, "Gagal Cetak");
						}
						
						tabelBeli.setModel(new DefaultTableModel(null, headBeli));
						textKembali.setText("");
						txtHarJum.setText("");
						txtBayar.setText("");
					}else {
						JOptionPane.showMessageDialog(null, "Terimakasih");
						tabelBeli.setModel(new DefaultTableModel(null, headBeli));
						textKembali.setText("");
						txtHarJum.setText("");
						txtBayar.setText("");
					}
				}else {
					JOptionPane.showMessageDialog(null, "Silahkan Isi Kembali");
				}
			}
		});
		btnCal.setBounds(567, 486, 89, 40);
		add(btnCal);		

		
		setVisible(false);
	}
	
	private void calculate() {
		if(txtBayar.getText() != null && !txtBayar.getText().trim().equalsIgnoreCase("")) {
			Integer ttal = Integer.parseInt(txtHarJum.getText());
			Integer pay = Integer.parseInt(txtBayar.getText());
			
			if(pay >= ttal ) {
				Integer kem = pay - ttal;
				textKembali.setText(""+kem);
			}else {
				txtBayar.setText("");
				textKembali.setText("");
				JOptionPane.showMessageDialog(null, "Mohon maaf Uang anda kurang");
			}
		}else {
			textKembali.setText("");
		}
	}
	
	private boolean validasi() {
		boolean valid = true;
		if(txtJumlah.getText().equalsIgnoreCase("")) {
			valid = false;
		}
		
		if(!valid) {
			JOptionPane.showConfirmDialog(null, "Silahkan isi berapa jumlah yang ingin di beli", "Gagal Menambahkan", JOptionPane.NO_OPTION);
		}
		return valid;
	}
	
	private void empty() {
		txtJumlah.setText("");
		txtBarang.setText("");
	}
}





