package com.gi.panel;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.gi.util.Constant;

public class AboutUsPanel extends JPanel {

	public AboutUsPanel() {
		setBackground(Color.LIGHT_GRAY);
		setBounds(0, 0, Constant.Panel_Width, Constant.Panel_Height);
		setLayout(null);
		setVisible(false);
		
		JLabel lblToko = new JLabel("MAJU JAYA SENTOSA");
		lblToko.setFont(new Font("Arial Black", Font.BOLD, 15));
		lblToko.setBounds(354, 61, 213, 22);
		add(lblToko);
		
		JEditorPane narasi = new JEditorPane();
		narasi.setBackground(Color.LIGHT_GRAY);
		narasi.setText("Visi\r\nMenjadi perusahaan besar yang terpandang, menguntungkan dan memiliki peran dominan dalam bisnis ini.\r\n\r\nMisi\r\n1. Menghasilkan laba yang pantas untuk mendukung pengembangan perusahaan.\r\n\r\n2.Memproduksi berbagai jenis mateial yang terkait dengan keinginan para konsumen dengan mutu, harga dan kualitas yang berdaya saing tinggi melalui pengelolaan yang profesional demi kepuasan pelanggan.\r\n\r\n3. Menjalin kemitraan kerja sama dengan pemasok dan penyalur yang saling menguntungkan.\r\n\r\n4. Menjadi perusahaan sepatu yang terbaik.\r\n");
		narasi.setFont(new Font("Arial", Font.BOLD, 14));
		narasi.setBounds(151, 110, 579, 289);
		add(narasi);
		
		JEditorPane cpr = new JEditorPane();
		cpr.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		cpr.setBackground(Color.LIGHT_GRAY);
		cpr.setForeground(new Color(0, 0, 0));
		cpr.setText("create by Muhammad Ikhsan");
		cpr.setBounds(161, 481, 406, 29);
		add(cpr);
	}

}
