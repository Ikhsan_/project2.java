package com.gi.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DataConnection {
	private Connection connection;
	private static DataConnection instance = new DataConnection();
	
	private DataConnection() {
		try {
			Class.forName(Constant.JDBC);
			
			System.out.println("Connecting to Database...");
			connection = DriverManager.getConnection(Constant.URL, Constant.USERNAME, Constant.PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static DataConnection getInstance() {
		return instance;
	}
	
	public Connection getConnection() {
		return this.connection;
	}
	
	public static void end(Connection conn) {
		
	}
	
	public static void close(PreparedStatement stmt, ResultSet rs) {
		try {
			if(rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
		}
	}
}
