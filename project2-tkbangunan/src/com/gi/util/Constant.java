package com.gi.util;

public class Constant {
	public static final Integer Panel_Width = 870;
	public static final Integer Panel_Height = 600;
	public static final String Satuan[] = {"kg", "pcs", "kubik"};
	public static final String Title = "TOKO BANGUNAN";
	
	public static final String USERNAME = "root";
	public static final String PASSWORD = "";
	public static final String JDBC = "com.mysql.cj.jdbc.Driver";
	public static final String URL = "jdbc:mysql://localhost:3306/tk_bangunan";
	
}
