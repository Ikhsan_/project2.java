package com.gi.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

public class Hellper {
	public static String currentDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		return formatter.format(new Date());
	}
	
	public static String generateCode() {
		String code =  UUID.randomUUID().toString();
		code = code.replaceAll("-", "");
		code = code.substring(0, 12);
		return code;
	}
	
	public static Long randomNumb() {
		Random numb = new Random();
		int n = numb.nextInt(100000)+1;
		Long val = Long.valueOf(n);
		return val;
	}
}
