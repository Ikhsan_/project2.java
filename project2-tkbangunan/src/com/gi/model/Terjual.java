package com.gi.model;

public class Terjual {
	private Long id_product;
	private String nama;
	private Long harga;
	private String satuan;
	private Long jumlah;
	private Long total;
	private String code;
	
	public Long getId_product() {
		return id_product;
	}
	public void setId_product(Long id_product) {
		this.id_product = id_product;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public Long getHarga() {
		return harga;
	}
	public void setHarga(Long harga) {
		this.harga = harga;
	}
	public String getSatuan() {
		return satuan;
	}
	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}
	public Long getJumlah() {
		return jumlah;
	}
	public void setJumlah(Long jumlah) {
		this.jumlah = jumlah;
	}
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	
	
}
