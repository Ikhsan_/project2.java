package com.gi.model;

import java.util.Date;

public class Transaksi {
	private Long id_transaksi;
	private Long id_product;
	private String nama_product;
	private String satuan;
	private Date tanggal;
	private String status;
	private Long jumlah;
	private Long harga;
	
	
	public Long getId_transaksi() {
		return id_transaksi;
	}
	public void setId_transaksi(Long id_transaksi) {
		this.id_transaksi = id_transaksi;
	}
	public Long getId_product() {
		return id_product;
	}
	public void setId_product(Long id_product) {
		this.id_product = id_product;
	}
	public String getNama_product() {
		return nama_product;
	}
	public void setNama_product(String nama_product) {
		this.nama_product = nama_product;
	}
	
	public String getSatuan() {
		return satuan;
	}
	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}
	public Date getTanggal() {
		return tanggal;
	}
	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getJumlah() {
		return jumlah;
	}
	public void setJumlah(Long jumlah) {
		this.jumlah = jumlah;
	}
	public Long getHarga() {
		return harga;
	}
	public void setHarga(Long harga) {
		this.harga = harga;
	}
}
