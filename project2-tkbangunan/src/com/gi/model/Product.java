package com.gi.model;

import java.util.Date;

public class Product {
	private Long id_produk;
	private Long id;
	private String nama_product;
	private Long stock;
	private Long harga_satuan;
	private Date tgl_masuk;
	private String supplier;
	private String satuan;
	private String deskripsi;
	private String code;
	
	public Long getId_produk() {
		return id_produk;
	}
	public void setId_produk(Long id_produk) {
		this.id_produk = id_produk;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNama_product() {
		return nama_product;
	}
	public void setNama_product(String nama_product) {
		this.nama_product = nama_product;
	}
	public Long getStock() {
		return stock;
	}
	public void setStock(Long stock) {
		this.stock = stock;
	}
	public Long getHarga_satuan() {
		return harga_satuan;
	}
	public void setHarga_satuan(Long harga_satuan) {
		this.harga_satuan = harga_satuan;
	}
	public Date getTgl_masuk() {
		return tgl_masuk;
	}
	public void setTgl_masuk(Date tgl_masuk) {
		this.tgl_masuk = tgl_masuk;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getSatuan() {
		return satuan;
	}
	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
}
