package com.gi.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.gi.model.Product;
import com.gi.model.Terjual;
import com.gi.model.Transaksi;
import com.gi.util.DataConnection;
import com.gi.util.ErrorHandler;
import com.gi.util.Hellper;

public class TransaksiDao implements BaseDao{

	@SuppressWarnings("unchecked")
	@Override
	public List getAllData() {
		List listTransaksi = new ArrayList();
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		try {
			String query = "SELECT * FROM tbl_transaksi";
			PreparedStatement ps = connection.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Transaksi transaksi = new Transaksi();
				transaksi.setId_transaksi(rs.getLong("id_transaksi"));
				transaksi.setId_product(rs.getLong("id_product"));
				transaksi.setNama_product(rs.getString("nama_product"));
				transaksi.setTanggal(rs.getDate("tanggal"));
				transaksi.setStatus(rs.getString("ket"));
				transaksi.setJumlah(rs.getLong("jumlah"));
				transaksi.setSatuan(rs.getString("satuan"));
				transaksi.setHarga(rs.getLong("total_harga"));
				listTransaksi.add(transaksi);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listTransaksi;
	}

	@Override
	public Object getDataById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getDataByVisibleCode(String code) {
		Product product = null;
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		try {
			String query = "SELECT * FROM tbl_product WHERE code = ?";
			System.out.println("ini code di dao: "+code);
			PreparedStatement ps = connection.prepareStatement(query);
			ps.setString(1, code);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				product = new Product();
				product.setId_produk(rs.getLong("id_product"));
				product.setId(rs.getLong("id"));
				product.setNama_product(rs.getString("nama_product"));
				product.setStock(rs.getLong("stock"));
				product.setHarga_satuan(rs.getLong("harga_satuan"));
				product.setTgl_masuk(rs.getDate("tgl_masuk"));
				product.setSupplier(rs.getString("supplier"));
				product.setSatuan(rs.getString("satuan"));
				product.setDeskripsi(rs.getString("Deskripsi"));
				product.setCode(rs.getString("code"));
			}
			DataConnection.close(ps, rs);
			DataConnection.end(connection);
		} catch (Exception e) {
			ErrorHandler.handleException("input Failed", e);
		}
		
		return product;
	}

	@Override
	public void deleteDataById(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveOrUpdate(Object obj) {
//		Transaksi trans = (Transaksi) obj;
		Terjual trans = (Terjual) obj;
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		String qry = null;
		String query = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		System.out.println("mampir");
		
		try {
			System.out.println("masuk");
			qry = 	"INSERT INTO tbl_transaksi"+
					"(id_transaksi, id_product, nama_product, tanggal, ket, jumlah, satuan, total_harga)"+
					"VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
			ps2 = connection.prepareStatement(qry);
			Long nump = Hellper.randomNumb();
			System.out.println("lanjut ke sini :" + nump);
			ps2.setLong(1, nump);
			ps2.setLong(2, trans.getId_product());
			ps2.setString(3, trans.getNama());
			ps2.setDate(4, new Date(System.currentTimeMillis()));
			ps2.setString(5, "Barang Keluar");
			ps2.setLong(6, trans.getJumlah());
			ps2.setString(7, trans.getSatuan());
			ps2.setLong(8, trans.getHarga()*trans.getJumlah());
			if(trans.getId_product() != null) {
				System.out.println("masuk tabel baru");
				
				query = "INSERT INTO tbl_stock"+
						"(id_product, stock)"+
						"VALUES(?, ?)";
				ps = connection.prepareStatement(query);
				ps.setLong(1, trans.getId_product());
				ps.setLong(2, trans.getJumlah());
			}
			
			ps.executeUpdate();
			ps2.executeUpdate();
			
			DataConnection.close(ps, null);
			DataConnection.close(ps2, null);
			DataConnection.end(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
