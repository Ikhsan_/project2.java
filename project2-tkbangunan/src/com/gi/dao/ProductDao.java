package com.gi.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.gi.model.Product;
import com.gi.util.DataConnection;
import com.gi.util.ErrorHandler;
import com.gi.util.Hellper;

public class ProductDao implements BaseDao{

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getAllData() {
		List listProduct = new ArrayList();
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		
		try {
			String query = "SELECT * FROM tbl_product ORDER BY tgl_masuk";
			PreparedStatement ps = connection.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Product product = new Product();
				product.setId_produk(rs.getLong("id_product"));
				product.setId(rs.getLong("id"));
				product.setNama_product(rs.getString("nama_product"));
				product.setStock(rs.getLong("stock"));
				product.setHarga_satuan(rs.getLong("harga_satuan"));
				product.setTgl_masuk(rs.getDate("tgl_masuk"));
				product.setSupplier(rs.getString("supplier"));
				product.setSatuan(rs.getString("satuan"));
				product.setDeskripsi(rs.getString("Deskripsi"));
				product.setCode(rs.getString("code"));
				listProduct.add(product);	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listProduct;
	}

	@Override
	public Object getDataById(Long id) {
		Product product = null;
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		try {
			String query = "SELECT * FROM tbl_product WHERE id_product = ?";
			
			PreparedStatement ps = connection.prepareStatement(query);
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				product = new Product();
				product.setId_produk(rs.getLong("id_product"));
				product.setId(rs.getLong("id"));
				product.setNama_product(rs.getString("nama_product"));
				product.setStock(rs.getLong("stock"));
				product.setHarga_satuan(rs.getLong("harga_satuan"));
				product.setTgl_masuk(rs.getDate("tgl_masuk"));
				product.setSupplier(rs.getString("supplier"));
				product.setSatuan(rs.getString("satuan"));
				product.setDeskripsi(rs.getString("Deskripsi"));
				product.setCode(rs.getString("code"));
			}
			DataConnection.close(ps, rs);
			DataConnection.end(connection);
		} catch (Exception e) {
			ErrorHandler.handleException("input Failed", e);
		}
		return product;
	}

	@Override
	public Object getDataByVisibleCode(String code) {
		Product product = null;
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		try {
			String query = "SELECT * FROM tbl_product WHERE code = ?";
			System.out.println("ini code di dao: "+code);
			PreparedStatement ps = connection.prepareStatement(query);
			ps.setString(1, code);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				product = new Product();
				product.setId_produk(rs.getLong("id_product"));
				product.setId(rs.getLong("id"));
				product.setNama_product(rs.getString("nama_product"));
				product.setStock(rs.getLong("stock"));
				product.setHarga_satuan(rs.getLong("harga_satuan"));
				product.setTgl_masuk(rs.getDate("tgl_masuk"));
				product.setSupplier(rs.getString("supplier"));
				product.setSatuan(rs.getString("satuan"));
				product.setDeskripsi(rs.getString("Deskripsi"));
				product.setCode(rs.getString("code"));
			}
			DataConnection.close(ps, rs);
			DataConnection.end(connection);
		} catch (Exception e) {
			ErrorHandler.handleException("input Failed", e);
		}
		return product;
	}

	@Override
	public void deleteDataById(Long id) {
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		try {
			String qry = "DELETE FROM tbl_product WHERE id_product = ?"+
						"DELETE FROM tbl_stock WHERE id_product = ?";
			PreparedStatement ps = connection.prepareStatement(qry);
			
			ps.setLong(1, id);
			ps.setLong(2, id);
			ps.executeUpdate();
			
			DataConnection.close(ps, null);
			DataConnection.end(connection);
		} catch (Exception e) {
			ErrorHandler.handleException("Failed", e);
		}
	}

	@SuppressWarnings("null")
	@Override
	public void saveOrUpdate(Object obj) {
		Product product = (Product) obj;
		DataConnection dataConnection = DataConnection.getInstance();
		Connection connection = dataConnection.getConnection();
		String query = null;
		String qry = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		try {
			if (product.getId() != null) {
				System.out.println("Data masuk skop ini");
				System.out.println(product.getId_produk());
				query = "UPDATE tbl_product SET " +
						"nama_product = ?, stock = ?, harga_satuan = ?, supplier = ? " +
						"WHERE id_product = ?";
				ps= connection.prepareStatement(query);
				ps.setString(1, product.getNama_product());
				ps.setLong(2, product.getStock());
				ps.setLong(3, product.getHarga_satuan());
				ps.setString(4, product.getSupplier());
				ps.setLong(5, product.getId_produk());
				
				
			}else {
				System.out.println("Data enter this skop");
				query = "INSERT INTO tbl_product" +
						"(id_product, nama_product, stock, harga_satuan, tgl_masuk, supplier, satuan, deskripsi, code)" +
						"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
				ps = connection.prepareStatement(query);
				String code = Hellper.generateCode();
				ps.setLong(1, product.getId_produk());
				ps.setString(2, product.getNama_product());
				ps.setLong(3, product.getStock());
				ps.setLong(4, product.getHarga_satuan());
				ps.setDate(5, new Date(System.currentTimeMillis()));
				ps.setString(6, product.getSupplier());
				ps.setString(7, product.getSatuan());
				ps.setString(8, product.getDeskripsi());
				ps.setString(9, code);
				
				if(product.getNama_product() != null) {
					qry = 	"INSERT INTO tbl_transaksi"+
							"(id_transaksi, id_product, nama_product, tanggal, ket, jumlah, satuan, total_harga)"+
							"VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
					ps2 = connection.prepareStatement(qry);
					Long nump = Hellper.randomNumb();
					System.out.println("lanjut ke sini :" + nump);
					ps2.setLong(1, nump);
					ps2.setLong(2, product.getId_produk());
					ps2.setString(3, product.getNama_product());
					ps2.setDate(4, new Date(System.currentTimeMillis()));
					ps2.setString(5, "Barang Masuk");
					ps2.setLong(6, product.getStock());
					ps2.setString(7, product.getSatuan());
					ps2.setLong(8, product.getHarga_satuan()*product.getStock());
				}
			}
			ps.executeUpdate();
			ps2.executeUpdate();
			
			DataConnection.close(ps, null);
			DataConnection.close(ps2, null);
			DataConnection.end(connection);
		} catch (Exception e) {
			ErrorHandler.handleException("Input Failed", e);
		}
		
	}

}
