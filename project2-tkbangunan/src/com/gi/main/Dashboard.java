package com.gi.main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.gi.dao.ProductDao;
import com.gi.model.Product;
import com.gi.panel.AboutUsPanel;
import com.gi.panel.HistoryPanel;
import com.gi.panel.InputProductPanel;
import com.gi.panel.ProductPanel;
import com.gi.panel.TerjualPanel;
import com.gi.util.Constant;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import java.awt.event.ActionEvent;

public class Dashboard extends JFrame {

	private JPanel masterPane;
	private JPanel panelDashboard;
	private JPanel panelAboutUs;
	private JPanel panelProduct;
	private JPanel panelInputProduct;
	private JPanel panelTerjual;
	private JPanel panelHistory;
	private JMenuBar menuBar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dashboard frame = new Dashboard();
					frame.setVisible(true);
					//frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Dashboard() {
		setTitle(Constant.Title);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(480, 35, 885, 660);
		
		
		try {
			//BufferedImage cover = ImageIO.read(new File("images/CvrBgn.jpg"));
			BufferedImage icon = ImageIO.read(new File("images/paluP.png"));
			BufferedImage bg = ImageIO.read(new File("images/CvrBgn.jpg"));
			setIconImage(icon);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu parent1 = new JMenu("File");
		menuBar.add(parent1);
		
		JMenuItem nmHome = new JMenuItem("Home");
		nmHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadDashboard();
			}
		});
		parent1.add(nmHome);
		
		JMenuItem nmAbout = new JMenuItem("About Us");
		nmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadAboutUs();
			}
		});
		parent1.add(nmAbout);
		
		JMenu parent2 = new JMenu("Produck");
		menuBar.add(parent2);
		
		JMenuItem nmInput = new JMenuItem("Input Product");
		nmInput.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadInputProduct();
			}
		});
		parent2.add(nmInput);
		
		JMenuItem nmProduct = new JMenuItem("Product");
		nmProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadProduct();
			}
		});
		parent2.add(nmProduct);
		
		JMenu parent3 = new JMenu("Transaction");
		menuBar.add(parent3);
		
		JMenuItem nmTerjual = new JMenuItem("Terjual");
		nmTerjual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadTerjual();	
			}
		});
		parent3.add(nmTerjual);
		
		JMenuItem nmHistory = new JMenuItem("History");
		nmHistory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadHistory();
			}
		});
		parent3.add(nmHistory);
		
		masterPane = new JPanel();
		masterPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(masterPane);
		masterPane.setLayout(null);
		
		panelDashboard = new JPanel();
		panelDashboard.setBackground(Color.LIGHT_GRAY);
		panelDashboard.setBounds(0, 0, Constant.Panel_Width, Constant.Panel_Height);
		panelDashboard.setVisible(true);
		masterPane.add(panelDashboard);
		panelDashboard.setLayout(null);
		
		JLabel titleDashboard = new JLabel("Toko Bangunan");
		titleDashboard.setFont(new Font("Arial Black", Font.BOLD, 18));
		titleDashboard.setBounds(373, 67, 168, 32);
		panelDashboard.add(titleDashboard);
		
		JLabel lblToko = new JLabel("MAJU JAYA SENTOSA");
		lblToko.setFont(new Font("Arial Black", Font.BOLD, 15));
		lblToko.setBounds(359, 106, 213, 22);
		panelDashboard.add(lblToko);
		
		JLabel lblBg = new JLabel(new ImageIcon(bg));
		lblBg.setBounds(0, 0, 870, 600);
		panelDashboard.add(lblBg);
		
		panelAboutUs = new AboutUsPanel();
		masterPane.add(panelAboutUs);
		
		panelProduct = new ProductPanel();
		masterPane.add(panelProduct);
		
		panelInputProduct = new InputProductPanel();
		masterPane.add(panelInputProduct);
		
		panelTerjual = new TerjualPanel();
		masterPane.add(panelTerjual);
		
		panelHistory = new HistoryPanel();
		masterPane.add(panelHistory);
		
		
		
//		ProductDao productDao = new ProductDao();
//		List listProduct = productDao.getAllData();
//		for (int i = 0; i < listProduct.size(); i++) {
//			Product product = (Product) listProduct.get(i);
//			System.out.println(product.getId_produk());
//		}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Read Image Failed", Constant.Title, JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		
	}
	
	public void loadDashboard() {
		panelDashboard.setVisible(true);
		panelAboutUs.setVisible(false);
		panelInputProduct.setVisible(false);
		panelProduct.setVisible(false);
		panelTerjual.setVisible(false);
		panelHistory.setVisible(false);
	}
	
	public void loadAboutUs() {
		panelDashboard.setVisible(false);
		panelAboutUs.setVisible(true);
		panelInputProduct.setVisible(false);
		panelProduct.setVisible(false);
		panelTerjual.setVisible(false);
		panelHistory.setVisible(false);
	}
	
	public void loadInputProduct() {
		panelDashboard.setVisible(false);
		panelAboutUs.setVisible(false);
		panelInputProduct.setVisible(true);
		panelProduct.setVisible(false);
		panelTerjual.setVisible(false);
		panelHistory.setVisible(false);
	}
	
	public void loadProduct() {
		panelDashboard.setVisible(false);
		panelAboutUs.setVisible(false);
		panelInputProduct.setVisible(false);
		panelProduct.setVisible(true);
		panelTerjual.setVisible(false);
		panelHistory.setVisible(false);
	}
	
	public void loadTerjual() {
		panelDashboard.setVisible(false);
		panelAboutUs.setVisible(false);
		panelInputProduct.setVisible(false);
		panelProduct.setVisible(false);
		panelTerjual.setVisible(true);
		panelHistory.setVisible(false);
	}
	
	public void loadHistory() {
		panelDashboard.setVisible(false);
		panelAboutUs.setVisible(false);
		panelInputProduct.setVisible(false);
		panelProduct.setVisible(false);
		panelTerjual.setVisible(false);
		panelHistory.setVisible(true);
	}
}
